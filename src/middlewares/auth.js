const { verify } = require("jsonwebtoken");

const auth = (req, res, next) => {
  const { authorization } = req.headers;
  if (authorization && authorization.startsWith("Bearer")) {
    const token = authorization.split(" ")[1];

    if (!token) {
      return res
        .status(400)
        .send({ message: "not authorized, please log-in again" });
    }
    const decodedToken = verify(token, process.env.JWT_SEC_KEY);
    req.decodedToken = decodedToken;
    next();
  } else {
    return res
      .status(400)
      .send({ message: "not authorized, please log-in again" });
  }
};

module.exports = auth;
