const { Schema, model } = require("mongoose");

const productSchema = new Schema(
  {
    productName: {
      type: String,
      required: true,
    },

    brandName: {
      type: String,
      required: true,
    },

    category:{
      type: String,
      required: true,
    },

    Advantages: {
      type: String,
    },

    description: {
      type: String,
      required: true,
    },

    MRP_Price: {
      type: Number,
      required: true,
    },

    current_Price: {
      type: Number,
      required: true,
    },

    discount: {
      type: String,
      default: "0%",
    },

    // isFreeShipping: {
    //   type: Boolean,
    //   default: false,
    // },

    // shippingCharge: {
    //   type: Number,
    //   default: 100,
    // },
  },

  { timestamps: true }
);

const Product = model("Product", productSchema);
module.exports = Product;
