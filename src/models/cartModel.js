const { Schema, model } = require("mongoose");

const cartSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },

  items: [
    {
      productId: {
        type: Schema.Types.ObjectId,
        required: true,
      },
      quantity: { type: Number, default: 1 },
      price: { type: Number, required: true },
    },
  ],
  totalItems: { type: Number, required: true },
  totalPrice: { type: Number, required: true },
});

const Cart = model("Cart", cartSchema);
module.exports = Cart;
