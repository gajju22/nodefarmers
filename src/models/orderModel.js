const { Schema, model } = require("mongoose");

//!not completed
const orderSchema = new Schema(
  {
    cartId: {
      type: Schema.Types.ObjectId,
      ref: "Cart",
    },

    address: {
      village: String,
      city: String,
      state: String,
      pinCode: Number,
    },

    payableAmount: {
      type: Number,
      required: true,
    },

    status: {
      type: String,
      enum: ["pending", "delivered", "canceled"],
    },
  },
  { timestamps: true }
);

const Order = model("Order", orderSchema);
