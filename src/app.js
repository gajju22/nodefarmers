const express = require("express");
const profileRoutes = require("./routes/farmersProfileRoute");
const signInRoutes = require("./routes/sign-inRoutes");
const productRoutes = require("./routes/productRoute");
const cartRoutes = require("./routes/cartRoute");

const app = express();

app.use(express.json());

//routes
app.use("/farmer", profileRoutes);
app.use("/farmer", signInRoutes);
app.use("/product", productRoutes);
app.use("/cart", cartRoutes);

module.exports = app;
