require("dotenv").config();
const connectToDb = require("./config/db");
const app = require("./app");

const port = process.env.PORT || 3000;

//db connection
connectToDb();

app.listen(port, () => console.log("express is connected in port " + port));
