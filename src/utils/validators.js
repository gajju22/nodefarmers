exports.isValid = (value) => {
  if (value === undefined || value === null) return false;
  if (typeof value === "string" && value.trim().length === 0) return false;
  return true;
};

exports.isValidBody = (body) => {
  return Object.keys(body).length !== 0;
};
