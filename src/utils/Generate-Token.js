const { sign } = require("jsonwebtoken");

const generateToken = (payload) => {
  return sign(payload, process.env.JWT_SEC_KEY);
};

module.exports = generateToken;
