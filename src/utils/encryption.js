const { hash } = require("bcrypt");

const encrypt = async (password, salt) => {
  const encryptedPassword = await hash(password, salt);
  console.log(encryptedPassword);
  return encryptedPassword;
};

module.exports = encrypt;
