const { Router } = require("express");
const router = Router();

const {
  registerProduct,
  showProduct,
} = require("../controllers/productController");


router.post("/register", registerProduct);
router.get("/:productId/details", showProduct);

module.exports = router;
