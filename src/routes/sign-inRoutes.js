const { Router } = require("express");
const router = Router();

const { signIn } = require("../controllers/Sign-in");

router.post("/sign-in", signIn);

module.exports = router;
