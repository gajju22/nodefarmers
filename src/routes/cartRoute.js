const { Router } = require("express");
const {
  addItems,
  removeItems,
  deleteCart,
} = require("../controllers/cartController");

const auth = require("../middlewares/auth");

const router = Router();

router.post("/:userId/add-items", auth, addItems);
router.put("/:userId/:productId/remove-items", auth, removeItems);
router.delete("/:userId/remove-cart", auth, deleteCart);

module.exports = router;
