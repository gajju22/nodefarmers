const { Router } = require("express");
const router = Router();

const { registerFarmers } = require("../controllers/profileController");

router.post("/register", registerFarmers);

module.exports = router;
