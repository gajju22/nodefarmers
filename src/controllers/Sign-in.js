const { compare } = require("bcrypt");
const Farmer = require("../models/farmerDoc.model");
const generateToken = require("../utils/Generate-Token");
const { isValid, isValidBody } = require("../utils/validators");

exports.signIn = async (req, res) => {
  try {
    const { body } = req;
    const { id, password } = body;

    if (!isValidBody(body)) {
      return res.status(400).send({ message: "please! fill required filled." });
    }

    if (!isValid(id)) {
      return res
        .status(400)
        .send({ message: "please enter your email or phone number." });
    }

    if (!isValid(password)) {
      return res.status(400).send({ message: "please enter your password." });
    }

    const mechCredentials = await Farmer.findOne({
      $or: [{ mobile: id }, { email: id }],
    });

    if (!mechCredentials) {
      return res
        .status(401)
        .send({ status: "invalid", message: "credentials not match" });
    }

    if (!(await compare(password, mechCredentials.password))) {
      return res
        .status(401)
        .send({ status: "invalid", message: "credentials not match" });
    }

    const payload = { id: mechCredentials._id };
    const token = generateToken(payload);
    res.status(203).send({
      status: "success",
      message: `${mechCredentials.firstName} successfully logged in.`,
      userId: mechCredentials._id,
      token: token,
    });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};
