const Cart = require("../models/cartModel");
const Product = require("../models/productModel");
const { isValidBody, isValid } = require("../utils/validators");

//add items in cart
exports.addItems = async (req, res) => {
  try {
    const { body, params, decodedToken } = req;
    const { userId, item, totalItems, totalPrice } = body;

    const createCart = { userId: params.userId };
    const items = [];

    if (decodedToken.id !== params.userId) {
      return res.status(403).send({
        status: "unauthorized",
        message: "not authorized please log in again!",
      });
    }

    if (!isValidBody(body)) {
      return res.status(400).send({ message: "please! fill required filled" });
    }

    // createCart["userId"] = params.userId;

    //cart item --->
    if (!item) {
      return res.status(400).send({ message: "add items" });
    }

    const { productId, quantity, price } = item;

    if (!isValid(productId)) {
      return res.status(400).send({ message: "product id is required" });
    }

    if (!isValid(quantity)) {
      return res.status(400).send({ message: "quantity is required" });
    }

    const product = await Product.findById(productId);
    const cart = await Cart.findOne({ userId: params.userId });

    item["price"] = product.current_Price;
    //cart item -->

    //cart already exist
    if (cart) {
      const newCart = JSON.parse(JSON.stringify(cart));
      const isProductExist = newCart.items.find(
        (el) => String(el.productId) == productId
      );

      //if product not exist in cart ->  add product in cart
      if (!isProductExist) {
        newCart.items.push(item);
        newCart.totalItems = newCart.items.length;
        newCart.totalPrice += product.current_Price * quantity;
        const updateCart = await Cart.findOneAndUpdate(
          { userId: newCart.userId },
          newCart,
          { new: true }
        );

        res.status(200).send({ status: true, cart: updateCart });
      } else {
        // if product exist -> inc. quantity
        console.log(1);
        isProductExist.quantity += quantity; //inc. prod quantity
        newCart.totalPrice += isProductExist.price * quantity; //inc. total price
        const updateCart = await Cart.findOneAndUpdate(
          { userId: newCart.userId },
          newCart,
          { new: true }
        );

        res.status(200).send({ status: true, cart: updateCart });
      }
    } else {
      // if cart not exist then creating cart for user
      items.push(item);
      createCart["items"] = items;
      createCart["totalItems"] = 1;
      createCart["totalPrice"] = product.current_Price * quantity;

      const saveCart = await Cart.create(createCart);
      res.status(201).send({ status: true, cart: saveCart });
    }
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//
//remove product from cart or dec. quantity
exports.removeItems = async (req, res) => {
  try {
    const { query, params, decodedToken } = req;
    const { userId, productId } = params;
    const { quantity } = query;

    if (decodedToken.id !== params.userId) {
      return res.status(403).send({
        status: "unauthorized",
        message: "not authorized please log in again!",
      });
    }

    const findCart = await Cart.findOne({ userId }); //finding user's cart

    if (!findCart) {
      return res.status(404).send({ status: false, message: "cart not exist" });
    }

    const cart = JSON.parse(JSON.stringify(findCart)); //deep copy

    //finding product from user's cart
    const product = cart.items.find(
      (product) => String(product.productId) === productId
    );

    if (!product) {
      return res
        .status(404)
        .send({ status: false, message: "product not exist" });
    }

    if (!isValid(quantity)) {
      return res.status(400).send({ message: "enter valid quantity" });
    }

    //if quantity is greater then actual prod quantity
    if (quantity > product.quantity) {
      //then throwing err
      return res.status(400).send({ message: "enter valid quantity" });
    }

    //quantity is same as product quantity
    // then remove that product
    if (Number(quantity) === product.quantity) {
      const prodIdx = cart.items.findIndex(
        (product) => String(product.productId) === productId
      );

      cart.items.splice(prodIdx, 1);
      cart.totalPrice -= product.price * product.quantity;
      cart.totalItems -= 1;

      //updating cart
      const updateCart = await Cart.findOneAndUpdate({ userId }, cart, {
        new: true,
      });

      res.status(200).send({ status: true, cart: updateCart });
      return;
    }

    // if remove quantity is less then product quantity
    product.quantity -= quantity; // dec. quantity
    cart.totalPrice -= product.price * quantity; // dec. total price.

    //updating cart
    const updateCart = await Cart.findOneAndUpdate({ userId }, cart, {
      new: true,
    });

    res.status(200).send({ status: true, cart: updateCart });
    return;
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//deleting cart
exports.deleteCart = async (req, res) => {
  try {
    const { params, decodedToken } = req;

    if (decodedToken.id !== params.userId) {
      return res.status(403).send({
        status: "unauthorized",
        message: "not authorized please log in again!",
      });
    }

    await Cart.deleteOne({ userId: params.userId });
    res.status(200).send({ status: true, message: "cart deleted" });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};
