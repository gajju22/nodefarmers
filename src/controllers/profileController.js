"use strict";
const encrypt = require("../utils/encryption");
const Farmer = require("../models/farmerDoc.model");
const { isValid, isValidBody } = require("../utils/validators");

//register user
exports.registerFarmers = async (req, res) => {
  try {
    const { body } = req;
    const { firstName, lastName, mobile, email, password, address } = body;

    // validations start----->
    // checking body is empty or not-
    if (!isValidBody(body)) {
      return res.status(400).send({ message: "please! fill required filled" });
    }

    //value validations-
    if (!isValid(firstName)) {
      return res.status(400).send({ message: "first name is required" });
    }

    if (!isValid(lastName)) {
      return res.status(400).send({ message: "last name is required" });
    }

    if (!isValid(mobile)) {
      return res.status(400).send({ message: "mobile number is required" });
    }

    //if user enter their email address-
    if (email || email === "") {
      // then check the value of email's property-
      if (!isValid(email)) {
        return res.status(400).send({ message: "enter valid email." });
      }
    }

    if (!isValid(password)) {
      return res.status(400).send({ message: "password  is mandatory" });
    }

    // password length-
    if (password.length > 15 || password.length < 8) {
      return res
        .status(400)
        .send({ message: "password length must between 8 to 15 " });
    }

    //password encryption
    const encryptedPassword = await encrypt(password, 10);
    body["password"] = encryptedPassword;

    const isNumberAlreadyUsed = await Farmer.findOne({ mobile });

    //number is used by another user-
    if (isNumberAlreadyUsed) {
      return res
        .status(400)
        .send({ message: "number already used please log-in ." });
    }

    if (!address) {
      return res.status(400).send({ message: "address is required" });
    }

    if (!isValidBody(address)) {
      return res.status(400).send({ message: "address is required" });
    }

    if (!isValid(address.village)) {
      return res.status(400).send({ message: "village is required." });
    }

    if (!isValid(address.city)) {
      return res.status(400).send({ message: "city is required." });
    }

    if (!isValid(address.state)) {
      return res.status(400).send({ message: "state is required." });
    }

    if (!isValid(address.pinCode)) {
      return res.status(400).send({ message: "pinCode is required." });
    }
    //validations end->

    const farmersProfile = await Farmer.create(body); //store data in db
    res.status(201).send({ status: "created👍🏻", profileData: farmersProfile });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};
