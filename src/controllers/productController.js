"use strict";

const Product = require("../models/productModel");
const { isValid, isValidBody } = require("../utils/validators");

exports.registerProduct = async (req, res) => {
  try {
    const { body } = req;

    const {
      productName,
      brandName,
      category,
      Advantages,
      description,
      MRP_Price,
      current_Price,
      discount,
      // isFreeShipping,
      // shippingCharge,
    } = body;

    const productData = {};

    if (!isValidBody(body)) {
      return res.status(400).send({ message: "please! fill required filled" });
    }

    if (!isValid(productName)) {
      return res.status(400).send({ message: "productName is required" });
    }
    productData["productName"] = productName; //storing data productData

    if (!isValid(brandName)) {
      return res
        .status(400)
        .send({ message: "enter product's brand name is required" });
    }
    productData["brandName"] = brandName;

    if (!isValid(category)) {
      return res.status(400).send({ message: "product category is required" });
    }
    productData["category"] = category;

    if (Advantages) {
      if (!isValid(Advantages)) {
        return res.status(400).send({ message: "enter valid advantages" });
      }
      productData["Advantages"] = Advantages;
    }

    if (!isValid(description)) {
      return res.status(400).send({ message: "description is required" });
    }
    productData["description"] = description;

    if (!isValid(MRP_Price)) {
      return res.status(400).send({ message: "MRP_Price is required" });
    }
    productData["MRP_Price"] = MRP_Price;

    if (!isValid(current_Price)) {
      return res.status(400).send({ message: "current_Price is required" });
    }
    productData["current_Price"] = current_Price;

    const discountPercent = 100 - (current_Price / MRP_Price) * 100;
    productData["discount"] =
      MRP_Price === current_Price
        ? `no discount with this product`
        : `you save ${Math.round(discountPercent)} %`;

    const saveProductData = await Product.create(productData);
    res.status(201).send({ status: "success", product: saveProductData });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//show single product->
exports.showProduct = async (req, res) => {
  try {
    const { productId } = req.params;

    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).send({ message: "product not found" });
    }
    res.status(200).send({ product });
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

//list all product  && list product by its category
exports.listProducts = (req, res) => {
  try {
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};
