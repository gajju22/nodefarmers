const mongoose = require("mongoose");

const connectToDb = async () => {
  try {
    const uri = process.env.DATABASE;
    await mongoose.connect(uri);
    console.log("mongo connected...");
  } catch (err) {
    res.sendStatus(500);
    console.log(err.message);
  }
};

module.exports = connectToDb;
